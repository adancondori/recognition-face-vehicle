#!/bin/bash


# OPTION 1: Recrear solo la imagen container_name: app
docker-compose up -d --force-recreate --no-deps --build app

# Eliminamos el cache de las imagene
#yes | docker system prune

# OPTION 2: No cache images
# docker-compose build --no-cache 
# docker-compose up --force-recreate --no-deps -d
