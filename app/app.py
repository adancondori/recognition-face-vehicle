from typing import List, Dict
from flask import Flask, request, make_response, render_template, flash, redirect, url_for, json
from flask_bootstrap import Bootstrap
import config
import service
import base64
import numpy as np
import cv2
import os
import logging 
from mysql.connector import Error
from mysql.connector import pooling
from detection_plate import Captura
import mysql.connector
import json
import time


app = Flask(__name__)
app.secret_key = "3d6f45a5fc12445dbac2f59c3b6c7cb1"

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)


Bootstrap(app)
    

config = {
    'user': 'root',
    'password': 'root',
    'host': 'db',
    'port': '3306',
    'database': 'facial'
}

def list() -> List[Dict]:
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute("""
        SELECT
            id as id,
            rostro as rostro, 
            concat(concat(nombre, ' '), apellidos ) as conductor, 
            matricula_vehiculo as vehiculo, 
            DATE_FORMAT(created_at, '%Y-%m-%d %H:%i:%s') as ingreso, 
            DATE_FORMAT(updated_at, '%Y-%m-%d %H:%i:%s') as salida 
        FROM personas;
        """)
    #results = [{nombre: apellidos} for (nombre, apellidos) in cursor]
    results = cursor.fetchall()
    cursor.close()
    connection.close()
    return results

@app.route('/add_face', methods=['POST'])
def add_face():
    userid = request.form["userid"]
    userface= request.form["face"]
    msg = service.save_img(userid, userface)
    return make_response(msg)

@app.route('/entrenar', methods=['POST'])
def entrenar():
    #msg = service.entrenar()
    #return json.dumps({'status':'ok'});
    return service.entrenar();

@app.route('/add_person', methods=['POST'])
def add_person():
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    
    if request.method == 'POST':
        fullname = request.form['nombre'].split(" ")
        name=''
        lastname=''
        for i in range(len(fullname)):
            if i == 0: 
                name = fullname[i]
            else:
                lastname += (" " + fullname[i])
                lastname = lastname.strip()
        sql = "INSERT INTO personas(rostro, nombre, apellidos, matricula_vehiculo) VALUES (%s, %s, %s, %s)"
        val = ('AS99912', name, lastname, 'DJDZ-57')
        cursor.execute(sql, val)
        connection.commit()
        cursor.close()
        flash('Persona agregada correctamente')
        return redirect(url_for('index'))


@app.route('/delete/<string:id>', methods= ['POST','GET'])
def delete(id):
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('DELETE FROM personas WHERE id = {0}'.format(id))
    connection.commit()
    msg = service.remove_img(id)
    flash('Registro eliminado correctamente! ' + msg)
    return redirect(url_for('index'))

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', list_person = list())

@app.route('/placa', methods=['GET'])
def placa():
    return render_template('placa.html', list_person = list())

@app.route('/validate', methods=['GET','POST'])
def validate():
    userid = request.form["userid"]
    userface= request.form["plate"]
    placa = "Error al leer"

    #ruta_image = r'image7.jpg'
    try:
        ruta_image = only_save_image(userface)
        obj = Captura(ruta_image)
        placa = obj.CapturaDePlacas()
    except:
        placa = "Error al leer"
    ruta_image = only_save_image(userface)
    obj = Captura(ruta_image)
    placa = obj.CapturaDePlacas()
    return placa
    


def only_save_image(image_base64):    
    timestr = time.strftime('%Y%m%d-%H%M%S')
    filename = "data/"+timestr+".jpg"
    image_binary = base64.b64decode(image_base64)
    image_jpg = np.frombuffer(image_binary, dtype=np.uint8)
    image = cv2.imdecode(image_jpg, cv2.IMREAD_COLOR)
    cv2.imwrite(filename, image)  
    return filename

#@app.route('/modelo_entrenado', methods=['GET'])
#def modelo_entrenado():
#    msg = "modelo.xml"
#    return make_response(msg)

@app.route('/capture_image', methods=['POST'])
def capture_image():
    msg = save_image(request.form["img"])
    return make_response(msg)

def save_image(image_base64):
    if not os.path.exists('data'):
        print('Carpeta creada: ','data')
        os.makedirs('data')

    timestr = time.strftime('%Y%m%d-%H%M%S')

    image_binary = base64.b64decode(image_base64)
    image_jpg = np.frombuffer(image_binary, dtype=np.uint8)
    image = cv2.imdecode(image_jpg, cv2.IMREAD_COLOR)
    image_file = "data/"+timestr+".jpg"
    cv2.imwrite(image_file, image)
    return "SUCCESS"

@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template("500.html"), 500

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)


