import base64
import numpy as np
import cv2
import time
import os
import json
import shutil

def save_img(id, img_base64):
    img_binary = base64.b64decode(img_base64)
    img_jpg = np.frombuffer(img_binary, dtype=np.uint8)
    img = cv2.imdecode(img_jpg, cv2.IMREAD_COLOR)    
    timestr = time.strftime('%Y%m%d_%H%M%S')
    dataPath = 'data/' + id + "/"
    if not os.path.exists(dataPath):
        os.makedirs(dataPath)
    image_file = dataPath + timestr + ".jpg"
    cv2.imwrite(image_file, img)
    return "save ok"

def remove_img(id):
    dataPath = 'data/' + id + "/"
    try:
        shutil.rmtree(dataPath, ignore_errors=True)
        msg="remove ok"
    except OSError as e:
        msg="Error: %s : %s" % (dataPath, e.strerror)
    return msg

def entrenar():
    dataPath = 'data'
    peopleList = os.listdir(dataPath)


    
    labels = []
    facesData = []
    label = 0

    for nameDir in peopleList:
        if not nameDir.startswith('.'):
            personPath = dataPath + '/' + nameDir
            for fileName in os.listdir(personPath):
                if not fileName.startswith('.'):
                    print('Rostros: ', nameDir + '/' + fileName)
                    labels.append(label)
                    facesData.append(cv2.imread(personPath + '/' + fileName, 0))
                    image = cv2.imread(personPath+'/'+fileName,0)
                    #cv2.imshow('image',image)
                    cv2.waitKey(10)
            label = label + 1

    #print('labels= ',labels)
    #print('Número de etiquetas 0: ',np.count_nonzero(np.array(labels)==0))
    #print('Número de etiquetas 1: ',np.count_nonzero(np.array(labels)==1))

    # Métodos para entrenar el reconocedor
    #face_recognizer = cv2.face.EigenFaceRecognizer_create()
    #face_recognizer = cv2.face.FisherFaceRecognizer_create()
    face_recognizer = cv2.face.LBPHFaceRecognizer_create()

    # Entrenando el reconocedor de rostros
    print("Entrenando...")
    face_recognizer.train(facesData, np.array(labels))

    # Almacenando el modelo obtenido
    #face_recognizer.write('modeloEigenFace.xml')
    #face_recognizer.write('modeloFisherFace.xml')
    face_recognizer.write('modeloEntrenado.xml')
    print("Modelo almacenado...")
    return json.dumps(peopleList)


