# Reconocimiento facial y vehicular

Proyeto de reconocimiento facial y reconocimiento de matricula vehicular.
Modelo y aprendizaje profundo con redes neuronales convulcionales con Tensor Flow, OpenCV, Python y Flask.

![Image text ](Captura_de_Pantalla_2021-11-05_a_la_s__13.59.42.png)

**Proyecto subido a GitLab:**
 - https://gitlab.tecnof5.cl/avcaceresd/reconocimiento-facial-y-vehicular

**Requerimientos:**
 - Instalacion docker (https://www.docker.com/get-started)
 - Python Python 3.6.15
 - Terminal Linux / Mac / Windows (powershell)

**Pasos de ambientacion y ejecucion:**
 - Paso 1: Instalacion de docker (guia de instalacion link: https://docs.docker.com/desktop/windows/install/)
 - Paso 2: Clonar repositorio http://gitlab.tecnof5.cl/avcaceresd/reconocimiento-facial-y-vehicular.git
 - Paso 3: En windows ejecutar el bat start.bat | Para Linux y Mac ejecutar desde terminal "sh start.sh" ó ./start.sh
 - Paso 4: Abrir navegador en la siguiente url: http://localhost:5001

 **Para ejecutar el proyecto sin docker:**
 
** MYSQL**
 - Paso 1: Instacion de Mysql 
 - Paso 2: Configurar credeciales, usuario y base de datos: 'user': 'root', 'password': 'root','host': 'db','port': '3306','database': 'facial'
 - Paso 3: Ejecutar y confirmar script de base de datos: "reconocimiento-facial-y-vehicular/bd/init.sql"

** FLASK**
 - Paso 1: Descargar https://gitlab.tecnof5.cl/avcaceresd/reconocimiento-facial-y-vehicular/-/archive/main/reconocimiento-facial-y-vehicular-main.zip
 - Paso 2: Descomprimir e Ingresar al directorio: "reconocimiento-facial-y-vehicular/app"
 - Paso 3: Instalar requerimientos del archivo requirements.txt con el comando "pip install"
 - Paso 4: Iniciar el servidor Flak comando: "python app.py"
 - Paso 5: Abrir navegador en la siguiente url: http://localhost:5001


**Posibles problemas al levantar el servicio:**
**FLASK:**
 - Puerto de red ocupado: Modificar archivo en reconocimiento-facial-y-vehicular/app/app.py cambiar el puerto en la linea "app.run(host='0.0.0.0', port=5001"


 
