REM verifica si existe los servicios ejecutando y los detiene y elimina
docker-compose down

REM crea e incia nuevos containers (servicios flask, db)
docker-compose up -d

REM verifica los estados de los servicios ejecutados recientemente
docker ps -a


REM verifica los logs del servidor flasks
docker logs app

pause