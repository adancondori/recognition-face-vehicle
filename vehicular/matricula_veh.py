import cv2
import pip

framewidth = 640
frameheight = 480

internalWebCam=0
externalWebCam=1

cap = cv2.VideoCapture(internalWebCam)
cap.set(3, framewidth)
cap.set(4, frameheight)
cap.set(10,150)

while True:
    success, img = cap.read()
    cv2.imshow("Result", img)
    if cv2.waitkey(1) & 0xFF == ord('q'):
        break

        
