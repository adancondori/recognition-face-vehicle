FROM python:3.6

EXPOSE 5000

WORKDIR /app

COPY ./app /app
RUN apt-get update
RUN apt-get install -y libgl1-mesa-dev
RUN pip install -r requirements.txt

CMD python app.py
