#!/bin/bash

# Bajamos los servicios (app - db)
docker-compose down

# Eliminamos los volumenes asignados a los contenedores.
yes | docker volume prune

# Eliminamos el cache de las imagene
yes | docker system prune

# yes | docker image prune --all

# Iniciamos un nuevo contenedor de los servios Flask and MariaDB.
docker-compose up -d
