CREATE DATABASE facial;
use facial;

CREATE TABLE personas (
  id int AUTO_INCREMENT PRIMARY KEY,
  rostro varchar(255) DEFAULT NULL,
  nombre varchar(100) DEFAULT NULL,
  apellidos varchar(100) DEFAULT NULL,
  matricula_vehiculo varchar(100) DEFAULT NULL,
  fecha date DEFAULT NULL,
  entrada time DEFAULT NULL,
  salida time DEFAULT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO personas(rostro, nombre, apellidos,matricula_vehiculo) 
VALUES
  ('AS99912', 'albert', 'caceres', 'DJDZ-57'),
  ('AS99912', 'juan jose', 'jose', 'TR-2569'),
  ('AS99912', 'edison', 'jose', 'XS-3341');